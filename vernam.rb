#!/usr/bin/env ruby
# SECURITE
# TP1
# REPAIN Paul
# DUT INFO
# 2018 - 2019

###############################################################################


def vernam(msg, key)
  """deciphers/ciphers a binary word with a key. The message and the key are
  both strings, that need to be cast into integers to do the xor"""
  len = key.length# avoid useless calculi inside the loop
  dec_msg = msg.clone
  for i in 0..(msg.length - 1)# for each bit, we do a xor with the key
    dec_msg[i] = (msg[i].to_i ^ key[i%len].to_i).to_s
  end
  dec_msg
end

###############################################################################

def main
  # User input
  msg = gets.chomp
  key = gets.chomp
  # Display deciphered message
  puts vernam(msg,key)
end

###############################################################################

main

