#!/usr/bin/env ruby
# SECURITE
# TP2
# REPAIN Paul
# DUT INFO
# 2018 - 2019


###############################################################################

def primary_decomposition(natural)
  """determines the primary decomposition of an natural number
  returns a hash"""
  i = 2# prime numbers are >= 2
  decomp = Hash.new
  decomp.default = 0# the default value is 0
  limit = Math.sqrt(natural)# divisors of natural are inferiror to its sqrt

  while i < limit or natural != 1
    if natural % i == 0# we see if natural is divisible by i
      decomp[i] += 1# we add a power to the prime number
      natural /= i
      # we can still divide by i, so we're not incrementing the value
    else
      i += 1# i not a divisor of natural, we move to the next number
    end
  end

  decomp
end

def phi(natural)
  """Euler's totient fonction, returns the count of positive integers up to
  a given integer natural, that are relatively prime to natural"""
  decomposition = primary_decomposition(natural)
  count = 1

  decomposition.each do |key, value|# key : prime number; value : power
    count *= key**value - key**(value - 1)
  end

  count
end

def euclidean_algorithm(a, b)
  """Euclidean algorithm, prints the divisors and the remainders, returns the
  rank p at which the remainder equals 0
  a the dividend, b the divisor"""
  if a < b or b <= 1
    return
  end
  p = -1
  remainder = -1

  while remainder != 0
    remainder = a % b
    print "B = ", b, " R = ", remainder, "\n"
    a = b# the dividend becomes the divisor
    b = remainder# and the divisor becomes the remainder
    p += 1
  end

  p
end


def inverse(x, modulus)
  """modular multiplicative inverse of an integer x"""
  a = x.abs
  b = modulus

  quotient = a / b
  remainder = a % b

  u1 = 1# Un
  u2 = 0# U(n+1)
  v1 = -quotient# Vn
  v2 = v1# V(n+1)

  while b > 0
    u2 = u1#
    v2 = v1#

    quotient = a / b
    remainder = a % b

    u1 = v2#
    v1 = u2 - v2 * quotient#

    a = b
    b = remainder
  end
  if u1 < 0
    u1 += modulus
  end

  u1
end

def remainder(x, p, n)
  """we convert p to a binary word, then for each 1, we do the calculation
  x, n and p natural integers not null"""
  product = 1
  i = 0
  bin = p.to_s(2).scan(/./)# convert to binary word, then to an array of chars

  bin.reverse_each { |c|
    if c == '1'
      power = 2**i
      product *= (x**power) % n
    end
    i += 1
  }

  product % n
end


def rsa(n, p, e, m)
  """m the message to cipher or decipher
  N = n * p
  d modular multiplicative inverse of e modulo phi(n)
  m' congruent to m**e[N] : the deciphered message
  m'' congruent to m'**d[N] <=> m'' = m : the message
  returns m'"""
  big_n = n * p
  phi_n = phi(big_n)
  d = inverse(e, phi_n)
  m_prime = remainder(m, e, big_n)
  m_double_prime = remainder(m_prime, d, big_n)
  print "M'' = ", m_double_prime,"\nM' = ", m_prime, "\n"

  m_prime
end








###############################################################################

def main
  puts "I"
  #natural = 51
  #decomposition = primary_decomposition(51)
  #decomposition2 = primary_decomposition(242)
  #puts decomposition
  #puts decomposition2
  #puts phi(phi(natural))
  #puts euclidean_algorithm(51, 242)
  puts "II"
  #euclidean_algorithm_v2(242, 51)
  #puts inverse(51, 242)
  puts "III"
  #remainder(4, 29, 91)
  puts "IV"
  rsa(101, 103, 7, 10331)
  rsa(7, 13, 5, 23)
end


###############################################################################

main
